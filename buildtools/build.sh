#!/bin/bash

PROJECT_NAME=properpy
PRO_FILE=$PROJECT_NAME.pro
TS_DIR=$PROJECT_NAME/i18n
LOCALES=fr_FR

cat <<EOF >$PRO_FILE
# This file was generated automatically.
# Any change made to this file will be lost on the next run of build.sh

EOF

for locale in $LOCALES
do
    echo "TRANSLATIONS += $TS_DIR/$locale.ts" >> $PRO_FILE
done

find . -name "*.qrc" -print | while read rc
do
    echo "Rcc'ing $rc..."
    pyrcc5 "$rc" -o "${rc/.qrc/_rc.py}"
    echo "RESOURCES += $rc" >> $PRO_FILE
done

find . -name "*.ui" -print | while read ui
do
    echo "Uic'ing $ui..."
    pyuic5 "$ui" -o "${ui/.ui/_ui.py}" --import-from=$PROJECT_NAME
    echo "FORMS += $ui" >> $PRO_FILE
done

echo "Listing source files..."
find . -name "*.py" -print | while read py
do
    if [[ "$py" != *"__init__"* && "$py" != *"_ui.py" ]]
    then
        echo "  - $py"
        echo "SOURCES += $py" >> $PRO_FILE
    fi
done

# pylupdate5 $PRO_FILE
# lrelease $PRO_FILE

# rm $PRO_FILE
