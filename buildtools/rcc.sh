#!/bin/bash

find . -name "*.qrc" -print | while read rc
do
    echo "Rcc'ing $rc..."
    pyrcc5 "$rc" -o "${rc/.qrc/_rc.py}"
done


