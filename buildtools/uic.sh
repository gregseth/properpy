#!/bin/bash

find . -name "*.ui" -print | while read ui
do
    echo "Uic'ing $ui..."
    pyuic5 "$ui" -o "${ui/.ui/_ui.py}" --import-from=demo
done
