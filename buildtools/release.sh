#!/usr/bin/env bash

QUERY_RESULT_FILE=ans.json
CURL="curl --silent --show-error --header Private-Token:$TOKEN --output $QUERY_RESULT_FILE"

# $1: the jq query string to execute if reply is a success
function get_api_result {
    error_msg=$(jq -r .message $QUERY_RESULT_FILE)
    if [[ "$error_msg" == "null" ]]
    then
        # default query is . (all)
        raw=""
        if [[ "$1" == "-r" ]]
        then
            raw="-r"
            shift
        fi
        query=$([[ -z "$1" ]] && echo '.' || echo "$1")
        jq $raw "$query" "$QUERY_RESULT_FILE"
        return 0
    else
        echo $error_msg
        return 1
    fi
}

project_url=$CI_API_V4_URL/projects/$CI_PROJECT_ID
echo "Project URL: $project_url"

# declaring array (mandatory)
declare -A links
# uploading whl and tar.gz and collecting their download links
for i in whl tar.gz
do
    file=$(ls dist/*.$i)
    $CURL --request POST --form "file=@$file" $project_url/uploads
    links[$i]=$(get_api_result -r .url) || exit 10
    echo "   - '$i' URL: $CI_PROJECT_URL${links[$i]}"
done

# testing if release exists, if so getting description and deleting the existing one
$CURL --request GET $project_url/releases/$CI_COMMIT_TAG
if description=$(get_api_result .description)
then
    echo "Pre-existing release found. Deleting..."
    $CURL --request DELETE $project_url/releases/$CI_COMMIT_TAG
    get_api_result || exit 20
    echo "Using description found in pre-existing release notes:"
else
    echo "Using default description:"
    description="$CI_PROJECT_NAME version ${CI_COMMIT_TAG#v}"
fi
echo $description

# create release with link to binary & source distribution
echo "Preparing query for release creation:"
tee release.json <<EOF
{
    "name": "$CI_PROJECT_NAME $CI_COMMIT_TAG",
    "tag_name": "$CI_COMMIT_TAG",
    "description": $description,
    "assets": {
        "links": [ {
            "name": "$CI_PROJECT_NAME $CI_COMMIT_TAG wheel package",
            "url": "$CI_PROJECT_URL${links[whl]}"
            }, {
            "name": "$CI_PROJECT_NAME $CI_COMMIT_TAG source distribution",
            "url": "$CI_PROJECT_URL${links[tar.gz]}"
        } ]
    }
}
EOF

echo "Requesting release creation..."
$CURL --request POST --header "Content-Type: application/json" --data @release.json $project_url/releases
get_api_result || exit 30
