import setuptools

import properpy

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as fh:
    install_requires = [line.strip() for line in fh]

setuptools.setup(
    name="properpy",
    version=properpy.__version__,
    author=properpy.__author__,
    author_email="gregory@millasseau.fr",
    description="A PyQt json file viewer",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/gregseth/properpy",
    packages=setuptools.find_packages(),
    install_requires=install_requires,
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
    ],
    entry_points={
        'console_scripts': [
            'properpy = properpy.demo.__main__:main'
        ]
    }
)
