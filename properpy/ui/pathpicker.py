import os
import sys

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget, QFileDialog, QApplication

from pathpicker_ui import Ui_PathPicker


BUTTON_OFFSET = 6


class PathPicker(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._ui = Ui_PathPicker()
        self._ui.setupUi(self)

    @pyqtSlot()
    def pick_path(self):
        base_dir = os.path.expanduser('~')
        current_dir = self._ui.pathLineEdit.text()
        if current_dir:
            if os.path.isdir(current_dir):
                base_dir = current_dir
            else:
                base_dir = os.path.dirname(current_dir)

        f = QFileDialog.getSaveFileName(
            self,
            base_dir,
            '',
            self.tr('All (*.*)')
        )[0]
        if f and f != self._ui.pathLineEdit.text():
            self._ui.pathLineEdit.setText(f)

#    def resizeEvent(self, event):
#        self._ui.pathLineEdit.resize(
#            event.size().width(),
#            self._ui.pathLineEdit.size().height()
#        )
#        self._ui.browseButton.move(
#            event.size().width() - self._ui.browseButton.size().width() + BUTTON_OFFSET
#        )


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = PathPicker()
    win.show()
    sys.exit(app.exec())
