import logging as log

from PyQt5.QtCore import Qt, QCoreApplication, QVariant
from PyQt5.QtGui import QStandardItemModel, QStandardItem

from . import DELIMITER

NAME_COL = 0
VALUE_COL = 1
RAWDATA_ROLE = Qt.UserRole + 1


def count_items(data):
    count = 0
    if not isinstance(data, dict):
        raise TypeError('Can only count from dict items.')
    for v in data.values():
        count += 1
        if isinstance(v, dict):
            count += count_items(v)
    return count


def flatten_dict(data, prefix=''):
    if not isinstance(data, dict):
        raise TypeError('Can only count from dict items.')
    flattened = dict()
    for k, v in data.items():
        key = '{}{}{}'.format(prefix, DELIMITER, k) if prefix else k
        if isinstance(v, dict):
            flattened[k] = k
            flattened.update(flatten_dict(v, key))
        else:
            flattened[key] = v
    return flattened


def get_index_for(index, collection):
    """ Returns index with the the correct type for the given collection """
    if isinstance(collection, list):
        return int(index)
    return index


class ProperpyModel(QStandardItemModel):
    HEADERS = [
        QCoreApplication.translate('ProperpyView', 'Name'),
        QCoreApplication.translate('ProperpyView', 'Value')
    ]

    def __init__(self, data, parent=None):
        """
        :param element: Object of which the properties will be listed
        """
        super().__init__(parent)
        self._data = data
        self._build_from_parent(self._data, self.invisibleRootItem())

    def _build_from_parent(self, data, parent):
        """
        Recursively build the item tree.
        Collections (dict, list, tuple, set) are displayed as subnodes of the
        tree. Unnamed collections (list, tuple, set) are automatically indexed,
        and the total count of their items show as value.
        :param data: The subset of the overall data to build
        :param parent: The parent QStandardItem for the data. Upon first call
            this is expected to be QStandardItemModel.invisibleRootItem.
        """
        if isinstance(data, dict):
            pairs = data.items()
        elif isinstance(data, list):
            pairs = enumerate(data)
        else:
            raise TypeError()

        for k, v in pairs:
            # excluding hidden properties
            if isinstance(k, str) and k[0] == '_':
                continue

            key = QStandardItem(str(k))
            key.setData(k)

            placeholder = QStandardItem()
            placeholder.setData(v)
            row = [key, placeholder]

            # manage here special display text according to data type, path, …
            if isinstance(v, (dict, list)):
                self._build_from_parent(v, key)

            parent.appendRow(row)

    def _path(self, model_index):
        """
        Returns a list ot the dict keys and list indexes required to get
        data of the given model_index from self._data
        """
        path = [model_index.siblingAtColumn(0).data()]
        # building path of dict/array keys
        while model_index.parent().isValid():
            model_index = model_index.parent()
            path.insert(0, model_index.data())
        return path

    def _data_from_index(self, model_index):
        """
        Returns the parent collection and the key to access the item in
        that collection corresponding to the given model_index
        """
        path = self._path(model_index)

        collection = self._data
        # unrolling keys to actual data to update target key
        for key in path[:-1]:
            collection = collection[get_index_for(key, collection)]
        keystring = get_index_for(path[-1], collection)

        return (collection, keystring)

    def columnCount(self, parent):
        # pylint: disable=unused-argument, no-self-use
        """ There's 2 columns in the table: Name and Value string"""
        return len(ProperpyModel.HEADERS)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        # pylint: disable=no-self-use
        """ The table column titles """
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return ProperpyModel.HEADERS[section]
            return section
        return QVariant()

    def data(self, model_index, role):
        value = super().data(model_index, role)
        if model_index.column() == VALUE_COL and role != RAWDATA_ROLE:
            # collecting the actual data
            coll, key = self._data_from_index(model_index)
            item_data = coll[key]

            if role == Qt.CheckStateRole:
                if isinstance(item_data, bool):
                    value = Qt.Checked if item_data else Qt.Unchecked
            elif role == Qt.DisplayRole:
                if isinstance(item_data, dict):
                    value = '{{{}}}'.format(len(item_data))
                elif isinstance(item_data, list):
                    value = '[{}]'.format(len(item_data))
                else:
                    value = str(item_data)
            elif role == Qt.EditRole:
                value = item_data
        # VERY verbose, enable only if necessary
        # log.debug('Model::data (%d, %d) value=%s(%s) role=%d', model_index.row(), model_index.column(), value, type(value), role)
        return value

    def flags(self, model_index):
        if not model_index.isValid():
            return Qt.NoItemFlags

        flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable

        if model_index.column() > 0:
            flags |= Qt.ItemIsEditable

        item_data = model_index.data(RAWDATA_ROLE)
        if isinstance(item_data, bool):
            # adding checkbox to the cell
            flags |= Qt.ItemIsUserCheckable
        elif isinstance(item_data, (dict, list)):
            # the cell is just showing the collection count
            flags = Qt.ItemIsSelectable

        return flags

    def setData(self, model_index, item_data, role):
        result = super().setData(model_index, item_data, role)

        if role == Qt.CheckStateRole:
            item_data = item_data == Qt.Checked

        log.debug('Model::setData (%d, %d) value=%s(%s) role=%d', model_index.row(), model_index.column(), item_data, type(item_data), role)
        if result and model_index.column() == VALUE_COL:
            # performing the actual data update
            coll, key = self._data_from_index(model_index)
            coll[key] = item_data

            log.debug('Model::setData %s <= %s', key, item_data)
            self.dataChanged.emit(model_index, model_index)

        return result
