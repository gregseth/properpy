from PyQt5.QtCore import Qt

from .base import ProperpyPlugin
from .modifier_picker import ModifierCheckboxes


class ModifiersPlugin(ProperpyPlugin):
    def __init__(self, items):
        super().__init__()
        self._items = items

    def widget(self, parent):
        checkboxes = ModifierCheckboxes(parent)
        # TODO maybe connect some signal to handle data change
        return checkboxes

    def set_editor_data(self, editor, index):
        # editor is assumed to be a ModifierCheckboxes
        editor.blockSignals(True)
        editor.set_values(index.data(Qt.EditRole))
        editor.blockSignals(False)

    def set_model_data(self, editor, model, index):
        # editor is assumed to be a ModifierCheckboxes
        model.setData(index, editor.values(), Qt.EditRole)
