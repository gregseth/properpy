from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QComboBox

from .base import ProperpyPlugin


class EnumPlugin(ProperpyPlugin):
    def __init__(self, items):
        super().__init__()
        self._items = items

    def widget(self, parent):
        combo = QComboBox(parent)
        combo.addItems(self._items)
        return combo

    def set_editor_data(self, editor, index):
        current_text = str(index.data(Qt.EditRole))
        position = editor.findText(current_text)
        if position >= 0:
            editor.blockSignals(True)
            editor.setCurrentIndex(position)
            editor.blockSignals(False)

    def set_model_data(self, editor, model, index):
        """ editor is the widget used to edit the data, here a QComboBox """
        model.setData(index, editor.currentText(), Qt.EditRole)
