class ProperpyPlugin():
    """ Base class for plugins """

    def __init__(self):
        """ Nothing to do """
        pass

    def widget(self, parent):
        """ Parent must be set as the parent of the returned widget """
        pass

    def set_editor_data(self, editor, index):
        """
        Assigns the data of the given index to the editor
        editor is the widget used to edit the data
        """
        pass

    def set_model_data(self, editor, model, index):
        """
        Assigns the data of the editor to the underlying model. When subclassing
        model.setData should be called for the appropriate role.
        editor is the widget used to edit the data
        """
        # TODO find appropriate role (Qt.EditRole?)
        pass
