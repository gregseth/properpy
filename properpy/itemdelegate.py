from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QStyledItemDelegate

from .plugins.base import ProperpyPlugin


class ProperpyItemDelegate(QStyledItemDelegate):
    """ Class handling the cell edition of the properties table """
    def __init__(self, parent=None):
        super().__init__(parent)

    def _plugin_for_path(self, path):
        for name, plugin in self._plugins:
            if path in plugin['paths']:
                return name
        return None

    def createEditor(self, parent, option, index):
        """ Creating the widget used as cell editor """
        plugin = index.data(Qt.EditRole)
        if isinstance(plugin, ProperpyPlugin):
            return plugin.widget(parent)

        return super().createEditor(parent, option, index)

    def setEditorData(self, editor, index):
        """ Definfing the data according to the cell editor type """
        plugin = index.data(Qt.EditRole)
        if isinstance(plugin, ProperpyPlugin):
            plugin.set_editor_data(editor, index)
        else:
            super().setEditorData(editor, index)

    def setModelData(self, editor, model, index):
        """ Sets the edited data to the model """
        plugin = index.data(Qt.EditRole)
        if isinstance(plugin, ProperpyPlugin):
            plugin.set_model_data(editor, model, index)
        else:
            super().setModelData(editor, model, index)
