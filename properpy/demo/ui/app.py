import os
import json

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox

from .app_ui import Ui_ProperpyApp
from ...model import ProperpyModel
from ...itemdelegate import ProperpyItemDelegate


class ProperpyApp(QMainWindow):
    def __init__(self, data, parent=None):
        super().__init__(parent)

        self._ui = Ui_ProperpyApp()
        self._ui.setupUi(self)

        self._data = data

        self._ui.properpyView.setModel(ProperpyModel(data, self))
        self._ui.properpyView.setItemDelegate(ProperpyItemDelegate(self))
        self._ui.properpyView.expandAll()

    @pyqtSlot()
    def dump_data(self):
        import pprint
        pprint.pprint(self._data)

    @pyqtSlot()
    def show_open_dialog(self):
        """ Opens the file open dialog """
        filename = QFileDialog.getOpenFileName(
            self,
            self.tr("Open"),
            '.',
            "Json (*.json *.js)"
        )[0]
        try:
            if filename and os.path.isfile(filename):
                with open(filename, 'r') as f:
                    self._ui.properpyView.set_data(json.load(f))
        except Exception as err:
            QMessageBox.warning(
                self,
                self.tr("Information"),
                self.tr("Error occured while loading the file:\n\n{}".format(err))
            )

    @pyqtSlot()
    def show_about_dialog(self):
        # TODO implement about dialog
        pass
