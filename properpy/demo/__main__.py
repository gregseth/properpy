import sys
import logging as log

from PyQt5.QtWidgets import QApplication, qApp
from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt, QDateTime, QFile

import properpy.demo as demo
from .ui.app import ProperpyApp


SAMPLE_DATA = {
    'string': 'Fear is the little death',
    'int': 42,
    'float': 3.141526,
    'boolean': True,
    '_hidden': 'thou shant see me',
    'array': ['this', 'is', 'a', 'list'],
    'collection': [{
        'name': 'Event',
        'type': 'MouseClick'
    }, {
        'name': 'Event',
        'type': 'KeyPressed'
    }],
    'dictionary': {
        'color': '#CCDD00',
        'qt_types': {
            'qcolor': QColor(Qt.red),
            'qdatetime': QDateTime(),
            'qpath': QFile('/Users/seth')
        },
        'timestamp': 1584540817,
        'datetime': '2020-03-20T10:00:00.666'
    }
}


def main():
    stream_handler = log.StreamHandler(sys.stdout)
    log.basicConfig(handlers=(stream_handler,),
                    level='DEBUG',
                    format='%(asctime)s.%(msecs)03d:%(levelname)-8s:%(module)-12s# %(message)s',
                    datefmt='%Y%m%d-%H%M%S'
                    )

    APP = QApplication(sys.argv)
    qApp.setApplicationName(demo.__appname__)
    qApp.setApplicationDisplayName(demo.__appname__)
    WIN = ProperpyApp(SAMPLE_DATA)

    WIN.show()
    sys.exit(APP.exec())


if __name__ == '__main__':
    main()
